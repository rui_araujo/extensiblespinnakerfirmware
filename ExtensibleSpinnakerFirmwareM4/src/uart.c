#include "chip.h"
#include "uart.h"



struct transmist_buffer tx_buffer;

/*****************************************************************************
 ** Function name:		UARTInit
 **
 ** Descriptions:		Initialize UART port, setup pin select,
 **						clock, parity, stop bits, FIFO, etc.
 **
 **
 *****************************************************************************/

void UARTInit(uint32_t baudrate) {
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT,RTS0_GPIO_PORT, RTS0_GPIO_PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT,CTS0_GPIO_PORT, CTS0_GPIO_PIN);

	Chip_UART_Init(LPC_USART0);
	Chip_UART_SetBaudFDR(LPC_USART0, baudrate);
	Chip_UART_TXEnable(LPC_USART0);
	tx_buffer.bufferReadPointer = 0;
	tx_buffer.bufferWritePointer = 0;
	UART0_READY();

}


