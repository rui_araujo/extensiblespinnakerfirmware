/*
 ===============================================================================
 Name        : main.c
 Author      : 
 Version     :
 Copyright   : Copyright (C) 
 Description : main definition
 ===============================================================================
 */

#include <string.h>
#include "chip.h"
#include "extra_pins.h"
#include "uart.h"
#include "spinnaker_boot.h"
#include "spinnaker_io.h"
#include "spinnaker_noc.h"
#include "spinnaker_p2p.h"
#include "command_protocol.h"
#include "cr_start_m0.h"

//Output State machine
#define IDLE						(0)
#define RECEIVE_ADDRESS				(1)
#define RECEIVE_ADDRESS_PAYLOAD		(2)
#define PARSE_BOOT_CMD				(100)
#define PARSE_SDP_CMD				(101)
#define SEND_PACKET					(103)
#define RECEIVING_BOOT_IMAGE		(200)
#define RECEIVING_SDP				(201)
#define RECEIVE_P2P_ADDRESS_1_BYTE	(300)
#define RECEIVE_P2P_ADDRESS_2_BYTE	(301)

uint8_t buffer[BLOCK_COUNT * BYTE_COUNT];
static uint8_t * sdp_buffer;

static union spin_packet_byte_accessible packet_from_app;

static uint32_t fast_atoi(uint8_t *p);

int main(void) {
	uint32_t state = IDLE;
	uint32_t image_size = 0;
	uint8_t received_char = 0;
	uint32_t index = 0;
	UARTInit(LOW_SPEED_BAUDRATE);
	reset_spinnaker();
	init_processing();
	// set P0.0 as output
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO);
	// set P0.1 as output
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO);

	// set P7.1 as input
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, SAVE_BOOT_PORT_GPIO, SAVE_BOOT_PIN_GPIO);

	// Start M0APP slave processor
	cr_start_m0(&__core_m0app_START__);

	if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, SAVE_BOOT_PORT_GPIO, SAVE_BOOT_PIN_GPIO)) {
		//TODO:read from EEPROM image to boot
	}

	while (1) {
		if (state == IDLE) {
			if (LPC_USART0->LSR & 0x01) {
				received_char = LPC_USART0->RBR;
				if (received_char == BOOT_CMD) {
					index = 0;
					state = PARSE_BOOT_CMD;
				} else if (received_char == SDP_CMD) {
					index = 0;
					state = PARSE_SDP_CMD;
				} else if (received_char == P2P_ADDRESS_CMD) {
					state = RECEIVE_P2P_ADDRESS_1_BYTE;
				} else if (received_char == PEEK_CMD || received_char == P2P_CMD || received_char == FIXED_ROUTE_CMD || received_char == NN_CMD
						|| received_char == MULTICAST_CMD) {
					index = 0;
					packet_from_app.buffer[index++] = received_char;
					state = RECEIVE_ADDRESS;
				} else if (received_char == POKE_CMD || received_char == P2P_PAYLOAD_CMD || received_char == FIXED_ROUTE_PAYLOAD_CMD
						|| received_char == NN_PAYLOAD_CMD || received_char == MULTICAST_PAYLOAD_CMD) {
					index = 0;
					packet_from_app.buffer[index++] = received_char;
					state = RECEIVE_ADDRESS_PAYLOAD;
				}
			}
		} else if (state == RECEIVE_P2P_ADDRESS_1_BYTE) {
			if (LPC_USART0->LSR & 0x01) {
				p2p_source_address = LPC_USART0->RBR << 16;
				state = RECEIVE_P2P_ADDRESS_2_BYTE;
			}
		} else if (state == RECEIVE_P2P_ADDRESS_2_BYTE) {
			if (LPC_USART0->LSR & 0x01) {
				p2p_source_address |= LPC_USART0->RBR << 24;
				state = IDLE;
			}
		} else if (state == RECEIVE_ADDRESS) {
			if (LPC_USART0->LSR & 0x01) {
				packet_from_app.buffer[index++] = LPC_USART0->RBR;
				if (index == 5) // Finished receiving the address
					state = SEND_PACKET;
			}
		} else if (state == RECEIVE_ADDRESS_PAYLOAD) {
			if (LPC_USART0->LSR & 0x01) {
				packet_from_app.buffer[index++] = LPC_USART0->RBR;
				if (index == 9) // Finished receiving the address and the payload
					state = SEND_PACKET;
			}
		} else if (state == SEND_PACKET) {
			UART0_BUSY();
			index = 0;
			switch (packet_from_app.packet.header) {
			case PEEK_CMD:
				send_peek(packet_from_app.packet.key);
				break;
			case P2P_CMD:
				send_p2p(packet_from_app.packet.key);
				break;
			case FIXED_ROUTE_CMD:
				send_fr(packet_from_app.packet.key);
				break;
			case NN_CMD:
				packet_from_app.packet.key &= 0x0fffffff; // Clean for the checksum
				packet_from_app.packet.key |= chksum_64(packet_from_app.packet.key, 0);
				send_nn(packet_from_app.packet.key);
				break;
			case MULTICAST_CMD:
				send_mc(packet_from_app.packet.key);
				break;
			case POKE_CMD:
				send_poke(packet_from_app.packet.key, packet_from_app.packet.payload);
				break;
			case P2P_PAYLOAD_CMD:
				packet_from_app.packet.payload |= chksum_32(packet_from_app.packet.payload);
				send_p2p_payload(packet_from_app.packet.key, packet_from_app.packet.payload);
				break;
			case FIXED_ROUTE_PAYLOAD_CMD:
				send_fr_payload(packet_from_app.packet.key, packet_from_app.packet.payload);
				break;
			case NN_PAYLOAD_CMD:
				packet_from_app.packet.key &= 0x0fffffff; // Clean for the checksum
				packet_from_app.packet.key |= chksum_64(packet_from_app.packet.key, packet_from_app.packet.payload);
				send_nn_payload(packet_from_app.packet.key, packet_from_app.packet.payload);
				break;
			case MULTICAST_PAYLOAD_CMD:
				send_mc_payload(packet_from_app.packet.key, packet_from_app.packet.payload);
				break;
			}
			state = IDLE;
			UART0_READY();
		} else if (state == PARSE_BOOT_CMD) {
			if (LPC_USART0->LSR & 0x01) {
				buffer[index++] = LPC_USART0->RBR;
				if (buffer[index - 1] == CMD_END) {
					UART0_BUSY();
					buffer[index - 1] = '\0';
					image_size = fast_atoi(buffer);
					index = 0;
					state = RECEIVING_BOOT_IMAGE;
					UART0_READY();
				}
			}
		} else if (state == PARSE_SDP_CMD) {
			if (LPC_USART0->LSR & 0x01) {
				buffer[index++] = LPC_USART0->RBR;
				if (buffer[index - 1] == CMD_END) {
					UART0_BUSY();
					buffer[index - 1] = '\0';
					image_size = fast_atoi(buffer);
					sdp_buffer = (uint8_t*) &tx_sdp.flags;
					index = 0;
					state = RECEIVING_SDP;
					UART0_READY();
				}
			}
		} else if (state == RECEIVING_BOOT_IMAGE) {
			//Wait for a character
			if (LPC_USART0->LSR & 0x01) {
				buffer[index++] = LPC_USART0->RBR;
				if (index >= image_size) {
					UART0_BUSY();
					//set the rest to 0
					memset(buffer + index, 0, sizeof(buffer) - index);
					boot_nn((uint32_t*) buffer);
					index = 0;
					/*if (GPIO_ReadValue(SAVE_BOOT_PORT_GPIO) & _BIT(SAVE_BOOT_PIN_GPIO)) {
					 //TODO:save the image to EEPROM
					 }*/
					state = IDLE;
					UART0_READY();
				}
			}
		} else if (state == RECEIVING_SDP) {
			//Wait for a character
			if (LPC_USART0->LSR & 0x01) {
				sdp_buffer[index++] = LPC_USART0->RBR;
				if (index >= image_size) {
					UART0_BUSY();
					//set the rest to 0
					//send_SDP
#ifdef DEBUG
					debug_p2p = 0; //off
#endif
					if (tx_desc.state == TX_IDLE) {
						tx_sdp.length = image_size;
						p2p_start_send_msg(&tx_sdp);
					}
					index = 0;
					state = IDLE;
					UART0_READY();
				}
			}
		}

		UART0_BUSY();
#ifndef LPC43_MULTICORE_M0APP
		if (((packetBufferWritePointer + 1) % PACKET_BUFFER_SIZE )!=packetBufferReadPointer) {
			parse_nibble(); // Check for input
		}
#endif
		if (packetBufferReadPointer != packetBufferWritePointer) {
			process_packet();
		}
		for (uint32_t j = 0; j < P2P_NUM_STR; ++j) {
			if (rx_desc_table[j].state != RX_IDLE) {
				p2p_rx_check_timeout(rx_desc_table + j);
			}
		}
		if (tx_desc.state != TX_IDLE) {
			p2p_continue_send_msg();
		}
		UART0_READY();

		/**
		 *Transmission
		 */
		if ((LPC_GPIO_PORT->PIN[RTS0_GPIO_PORT] & _BIT(RTS0_GPIO_PIN)) == 0) { // no rts stop signal
			while ((tx_buffer.bufferReadPointer != tx_buffer.bufferWritePointer) && (LPC_USART0->LSR & UART_LSR_THRE)) {
				LPC_USART0->THR = tx_buffer.buffer[tx_buffer.bufferReadPointer];
				tx_buffer.bufferReadPointer = (tx_buffer.bufferReadPointer + 1) & TX_BUFFER_MASK;
			}
		}
	}

	return 0;
}

static uint32_t fast_atoi(uint8_t *p) {
	uint32_t x = 0;
	while (*p != '\0') {
		x = (x * 10) + (*p - '0');
		++p;
	}
	return x;
}
