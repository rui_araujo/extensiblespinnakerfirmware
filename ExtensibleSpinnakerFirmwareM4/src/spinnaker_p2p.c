/*
 * spinnaker_p2p.c
 *
 *  Created on: 24 de Nov de 2013
 *      Author: ruka
 */
#include "spinnaker.h"
#include "spinnaker_p2p.h"
#include "spinnaker_io.h"
#include "spinnaker_noc.h"
#include "command_protocol.h"
#include "uart.h"

#define OPEN_REQ_RETRY 	(16) // Tx open_req
#define OPEN_ACK_RETRY  (4) // Rx open_ack
#define DATA_ACK_RETRY  (4) // Rx data_ack
#define CLOSE_REQ_RETRY (4) // Rx close_req
#define OPEN_ACK_TIME	(250) // Tx awaiting open_ack
#define DATA_ACK_TIME 	(3000) // Tx awaiting data ack
#define DATA_TIME 		(500)// Rx awaiting data
#define CLOSE_ACK_TIME 	(250) // Rx awaiting close_ack
tx_desc_t tx_desc;
rx_desc_t rx_desc_table[P2P_NUM_STR];

sdp_msg_t tx_sdp;

volatile uint32_t tx_timeout = 0;
volatile uint32_t rx_timeout = 0;

extern uint32_t chksum_32(uint32_t a);

static void p2p_send_pkt(uint32_t addr, uint32_t data) {
	if (addr & BIT_31)
		send_nn_payload(NN_SDP_KEY, data);
	else
		send_p2p_payload(addr, data);
}

static void p2p_send_ctl(uint32_t ctrl, uint32_t addr, uint32_t data) {
	data |= ctrl;
	data |= chksum_32(data);
	p2p_send_pkt(addr, data);
}

static uint32_t ipsum(uint8_t *d, uint32_t len, uint32_t sum) {
	if (len & 1)
		sum += d[--len] << 8;

	for (uint32_t i = 0; i < len; i += 2)
		sum += (d[i] << 8) + d[i + 1];

	while (sum >> 16)
		sum = (sum >> 16) + (sum & 0xffff);

	return sum;
}
//     	+-------+-----+-+---------------+---------------+-----+---------+
// 	  	|       |     | |                               |     |         |
//Open	|  Sum  | 1xx |1|            Length             | SQL |   TID   |
//Req  	|       |     | |                               |     |         |
// 	  	+-------+-----+-+---------------+---------------+-----+---------+
//	    +-------+-----+-+---+-+---------+---------------+---------------+
//		|       |     | |   | |         |               |               |
//Open	|  Sum  | 000 |1|RID|0|   TID   |               |      RC       |
//Ack  	|       |     | |   | |         |               |               |
//		+-------+-----+-+---+-+---------+---------------+---------------+

void p2p_open_req(uint32_t addr, uint32_t data) {
	uint32_t len = (data >> 8) & 0xffff; // Real length from SDP hdr on...
	uint32_t ctrl = data & 255;

	//the source is the destination of our packets
	addr >>= 16;
	uint32_t tid = ctrl & 31;
	uint32_t seq_len = 1 << (ctrl >> 5);

	if (len > (BUF_SIZE + 8 + 16) || seq_len > 16) //const
			{
		p2p_send_ctl(P2P_OPEN_ACK, addr, (tid << 16) + RC_P2P_REJECT);
		return;
	}

	rx_desc_t *desc = rx_desc_table;
	uint32_t rid = P2P_NUM_STR;

	for (uint32_t i = 0; i < P2P_NUM_STR; i++) {
		if (desc->state == RX_OPEN && desc->srce == addr && desc->tid == tid) { // Already open
			p2p_send_ctl(P2P_OPEN_ACK, addr, (tid << 16) + (i << 22) + RC_OK);

			desc->tcount = OPEN_ACK_RETRY;
			rx_timeout = DATA_TIME;

			return;
		}

		if (desc->state == RX_IDLE) // Free ?
			rid = i;

		desc++;
	}

	if (rid == P2P_NUM_STR) { // No free streams - send busy
		p2p_send_ctl(P2P_OPEN_ACK, addr, (tid << 16) + RC_P2P_BUSY);
		return;
	}

	desc = rx_desc_table + rid;
	desc->state = RX_OPEN;

//  desc->len = len;
	desc->ctrl = ctrl;
	desc->srce = addr;
	desc->tid = tid;
	desc->seq_len = seq_len;
	desc->done = 0;
	desc->phase = 0;

	desc->mask = desc->new_mask = (1 << seq_len) - 1;

	uint8_t *ptr = desc->base = (uint8_t *) &(desc->msg.flags) - 2;
	desc->limit = ptr + len + 4 - (2 + 3); // 2 for length & 3 per packet

	ptr[-2] = len;
	ptr[-1] = len >> 8;

	p2p_send_ctl(P2P_OPEN_ACK, addr, (tid << 16) + (rid << 22) + RC_OK);

	desc->tcount = OPEN_ACK_RETRY;
	rx_timeout = DATA_TIME;
}

// Received OPEN_ACK packet. If RX_BUSY do nothing so that timeout
// will expire (and we keep trying). Otherwise update tx_desc and
// cancel timeout.

void p2p_open_ack(uint32_t srce, uint32_t data) {
	uint32_t tid = (data >> 16) & 31;
	uint32_t rc = data & 255;
	//the source is the destination of our packets
	srce >>= 16;
	tx_desc_t *desc = &tx_desc;

	if (desc->state == TX_OPEN_REQ && desc->tid == tid&& desc->dest == srce
	&& rc != RC_P2P_BUSY) {
		uint32_t rid = (data >> 22) & 3;

		desc->rid = rid;
		desc->rc = rc;
		desc->ack = 1; //const

	}
}

void p2p_move_msg_uart(rx_desc_t *desc) {
	uint16_t len = desc->msg.length;
	if (free_space_for(len + 2)) {
		pushByte(&tx_buffer, ( (len >>8) &0x80)|SDP_CMD);
		pushByte(&tx_buffer, (uint8_t)len);
		uint8_t * buf = (uint8_t *) &(desc->msg.flags);
		for (uint16_t i = 0; i < len; ++i, ++buf) {
			pushByte(&tx_buffer, *buf);
		}
		desc->state = RX_IDLE;
	}
}

void p2p_close_ack(uint32_t srce, uint32_t data) {
	uint32_t rid = (data >> 22) & 3;
	rx_desc_t *desc = rx_desc_table + rid;
	//the source is the destination of our packets
	srce >>= 16;

	uint32_t tid = (data >> 16) & 31;

	if (desc->state == RX_CLOSE_REQ && desc->tid == tid && desc->srce == srce) {
		desc->state = RX_SENDING;
		p2p_move_msg_uart(desc);
	}
}

// Received CLOSE_REQ from receiver. If TX_OPEN_ACK then tidy up tx_desc.
// In any case, send a CLOSE_ACK back to receiver.

void p2p_close_req(uint32_t srce, uint32_t data) {
	uint32_t tid = (data >> 16) & 31;
	tx_desc_t *desc = &tx_desc;
	//the source is the destination of our packets
	srce >>= 16;

	if (desc->state == TX_OPEN_ACK && desc->tid == tid && desc->dest == srce) {
		desc->mask = 0;
		desc->state = TX_IDLE;
		desc->ack = 3; //const
	}

	p2p_send_ctl(P2P_CLOSE_ACK, srce, data & 0x00ffffff);
}
// Received ACK from receiver. Cancel ack timeout and update tx_desc

//      +-------+-----+-+---+-+---------+---------------+---------------+
//		|       |     | |   | |         |                               |
//Data	|  Sum  | 001 |1|RID|0|   TID   |           Ack mask            |
//Ack  	|       |     | |   | |         |                               |
//		+-------+-----+-+---+-+---------+---------------+---------------+

void p2p_data_ack(uint32_t srce, uint32_t data) {
	uint32_t tid = (data >> 16) & 31;
	uint32_t phase = (data >> 21) & 1;
	tx_desc_t *desc = &tx_desc;
	//the source is the destination of our packets
	srce >>= 16;

	if (desc->state == TX_OPEN_ACK && desc->tid == tid && desc->dest == srce) {

		if (phase != desc->phase) {
			desc->phase = phase;
			desc->base += 3 * desc->seq_len;
		}

		desc->mask = data;
		desc->ack = 1;
	}
}

// Timed out waiting for "seq_len" data packets. Send P2P_DATA_ACK
// with current mask and restart timeout.

void p2p_data_timeout(rx_desc_t *desc) {

	if (desc->state == RX_OPEN) {
		if (desc->tcount == 0) {
			desc->state = RX_IDLE;
			return;
		}

		desc->tcount--;
		p2p_send_ctl(P2P_DATA_ACK, desc->srce,
				(desc->phase << 21) + (desc->tid << 16) + desc->mask);
		rx_timeout = DATA_TIME;
	}
}

void p2p_rx_check_timeout(rx_desc_t *desc) {
	if (desc->state == RX_SENDING) {
		p2p_move_msg_uart(desc);
		return;
	}
	if (desc->state == RX_OPEN && rx_timeout == 0) {
		p2p_data_timeout(desc);
		return;
	}
	if (desc->state == RX_CLOSE_REQ && rx_timeout == 0) {
		p2p_close_timeout(desc);
		return;
	}
}

// Timed out waiting for CLOSE_ACK. Repeat P2P_CLOSE_REQ a few times
// then give up
// State should be RX_CLOSE_REQ
void p2p_close_timeout(rx_desc_t *desc) {
	if (desc->tcount == 0) {
		desc->state = RX_IDLE;
		return;
	}

	desc->tcount--;
	p2p_send_ctl(P2P_CLOSE_REQ, desc->srce,
			(desc->rid << 22) + (desc->tid << 16));
	rx_timeout = CLOSE_ACK_TIME;
}

//		 +---+---------+-+---------------+---------------+---------------+
// 	 	 |   |         | |                                               |
//Data	 |RID| Seq_num |0|                     [24]               	     |
// 	 	 |   |         | |                                               |
// 	 	 +---+---+-----+-+---------------+---------------+---------------+

void p2p_start_send_msg(sdp_msg_t *msg) {
	uint8_t *buf = (uint8_t *) &msg->length; // Point to len/sum
	uint32_t len = msg->length; // 'Real' length

	msg->flags |= SDPF_SUM; // Set checksum bit
	msg->checksum = 0; // buf[3..2] (BE)

	tx_desc_t *desc = &tx_desc;
	desc->len = len;
	desc->tid = (desc->tid + 1) & 31;

	uint32_t seq_len_log = P2P_DEF_SQL & 7;
	uint32_t ctrl = (seq_len_log << 5) + desc->tid;

	uint32_t sum = ~ipsum(buf, len + 4, 0); // NB "ctrl" omitted from sum

	buf[2] = sum >> 8; // Sum (hi)
	buf[3] = sum; // Sum (lo)

	desc->seq_len = 1 << seq_len_log;
	desc->seq = 0;
	desc->dest = msg->dest_addr;
	desc->base = buf + 2;
	desc->limit = buf + len - 3 + 4;
	desc->done = 0;
	desc->phase = 0;

	desc->state = TX_OPEN_REQ;
	desc->tcount = OPEN_REQ_RETRY;
	desc->rc = RC_P2P_NOREPLY;
	desc->ack = 0;
	p2p_send_ctl(P2P_OPEN_REQ, desc->dest, (len << 8) + ctrl);
	tx_timeout = OPEN_ACK_TIME;

}

void p2p_continue_send_msg() {

	tx_desc_t *desc = &tx_desc;
	if (desc->state == TX_OPEN_REQ) {
		uint32_t seq_len_log = P2P_DEF_SQL & 7;
		uint32_t ctrl = (seq_len_log << 5) + desc->tid;
		if (desc->ack == 0) {
			if (tx_timeout == 0) {
				//Keep track of the retries  count
				if (desc->tcount == 0) {
					desc->state = TX_IDLE;
					return;
				}
				desc->tcount--;
				desc->ack = 0;
				p2p_send_ctl(P2P_OPEN_REQ, desc->dest, (desc->len << 8) + ctrl);
				tx_timeout = OPEN_ACK_TIME;
			}
			return; //waiting for ACK
		}

		if (desc->rc != RC_OK) {
			desc->state = TX_IDLE;
			return; //TODO: send the errors back to the computer application
		}

		desc->state = TX_OPEN;
		desc->mask = (1 << desc->seq_len) - 1;
	}
	if (desc->state == TX_OPEN_ACK) {
		if (desc->ack == 0) {
			if (tx_timeout == 0) {
				desc->state = TX_IDLE; // Force exit
				desc->mask = 0;
				desc->done = 1;
				desc->ack = 2;
				desc->rc = RC_P2P_TIMEOUT;
			}
			return;  //waiting for ACK
		}
		desc->state = TX_OPEN;
	}
	if (desc->state == TX_OPEN) {
		uint32_t mask = desc->mask;

// !! needed  desc->tcount = RETRY; //const
		for (uint32_t seq = 0; seq < desc->seq_len; seq++) {
			uint8_t *p = desc->base + 3 * seq;

			if (mask & 1) {
				uint32_t data = (desc->rid << 30) + (desc->phase << 29)
						+ (seq << 25) + (p[2] << 16) + (p[1] << 8) + p[0];

				p2p_send_pkt(desc->dest, data);
			}

			if (p >= desc->limit) {
				desc->done = 1;
				break;
			}

			mask = mask >> 1;

			if (mask == 0)
				break;
		}

		desc->ack = 0;
		tx_timeout = DATA_ACK_TIME;
		desc->state = TX_OPEN_ACK;
	}
}

// May write 1 or 2 bytes beyond end of buffer (buffer has pad word)
// Could write further if error in "seq" field? - check!
// Rearrange base/limit to write at -1, -2, -3  then can
// save if ptr < limit

//       +---+---------+-+---------------+---------------+---------------+
// 	 	 |   |         | |                                               |
// Data	 |RID| Seq_num |0|                     [24]                      |
// 	 	 |   |         | |                                               |
// 	 	 +---+---+-----+-+---------------+---------------+---------------+

void p2p_data_pkt(uint32_t addr, uint32_t data) {
	uint32_t rid = (data >> 30) & 3;
	uint32_t phase = (data >> 29) & 1;
	rx_desc_t *desc = rx_desc_table + rid;
	//the source is the destination of our packets
	addr >>= 16;
	desc->rid = rid;
	if (desc->state == RX_OPEN && desc->srce == addr && desc->phase == phase) {
		uint32_t seq = (data >> 25) & (desc->seq_len - 1);
		uint8_t *ptr = desc->base + (3 * seq);

		ptr[0] = data;
		ptr[1] = data >> 8;
		ptr[2] = data >> 16;

		uint32_t seq_bit = 1 << seq;
		desc->mask &= ~seq_bit;

		if (ptr >= desc->limit) {
			desc->mask &= seq_bit - 1;
			desc->done = 1;
		}

		if (desc->mask == 0) { // Advance
			if (desc->done) { // Close
				desc->state = RX_CLOSE_REQ;
				desc->tcount = CLOSE_REQ_RETRY;

				p2p_send_ctl(P2P_CLOSE_REQ, desc->srce,
						(rid << 22) + (desc->tid << 16));

				// start close timout - mustn't exit until have CLOSE_ACK
				rx_timeout = CLOSE_ACK_TIME;
			} else { // more to do
				uint32_t phase = desc->phase ^= 1;
				desc->base += (3 * desc->seq_len);
				desc->mask = desc->new_mask;

				p2p_send_ctl(P2P_DATA_ACK, desc->srce,
						(phase << 21) + (desc->tid << 16) + desc->mask);

				desc->tcount = DATA_ACK_RETRY;
				rx_timeout = DATA_TIME;
			}
		}
	}
}

