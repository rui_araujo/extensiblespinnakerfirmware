/*
 * spinnaker_noc.c
 *
 *  Created on: Oct 23, 2013
 *      Author: raraujo
 */
#include "chip.h"
#include "spinnaker.h"
#include "spinnaker_io.h"
#include "spinnaker_noc.h"
#include "spinnaker_p2p.h"
#include "extra_pins.h"
#include "uart.h"
#include "command_protocol.h"

/*Buffer to indicate if a packet in the buffer is to be printed out or not*/
static uint32_t packets_to_be_printed[PACKET_BUFFER_SIZE >> 5];
/*This contains the current processed packet pointer*/
static uint32_t shadowBufferReadPointer = 0;
static struct spin_packet * packet;
static void p2p_rcv_pkt(uint32_t addr, uint32_t data);

#define PRINT_PACKET()  {packets_to_be_printed[shadowBufferReadPointer >> 5] |=_BIT(shadowBufferReadPointer%32);}

#define PRINT_DEBUG_PACKET()
#ifdef DEBUG
#undef PRINT_DEBUG_PACKET
#define PRINT_DEBUG_PACKET() \
{	if (debug_p2p == 0)\
	packets_to_be_printed[shadowBufferReadPointer >> 5] |=_BIT(shadowBufferReadPointer%32);\
}
#endif

void SysTick_Handler(void) {
	static uint32_t led_toogle = 0;
	if (led_toogle++ >= 1000) {
		Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, LED0_PORT_GPIO, LED0_PIN_GPIO); //Toggle LED
		led_toogle = 0;
	}
	if (tx_timeout != 0)
		tx_timeout--;
	if (rx_timeout != 0)
		rx_timeout--;
}

void init_processing() {
	tx_timeout = 0;
	rx_timeout = 0;
	uint32_t load = Chip_Clock_GetRate(CLK_MX_MXCORE) / 1000 - 1;
	if (load > 0xFFFFFF) {
		load = 0xFFFFFF;
	}
	SysTick->LOAD = load;
	SysTick->CTRL |= 0x7;	//enable the Systick
	shadowBufferReadPointer = 0;
	tx_desc.state = TX_IDLE;
	for (uint32_t i = 0; i < P2P_NUM_STR; ++i) {
		rx_desc_table[i].state = RX_IDLE;
	}
	for (uint32_t i = 0; i < sizeof(packets_to_be_printed); ++i) {
		packets_to_be_printed[i] = 0;
	}
	init_links();
}
void process_packet() {
	if (shadowBufferReadPointer != packetBufferWritePointer) {
		packet = &packets[shadowBufferReadPointer].packet;

		uint32_t type = packet->header & PKT_MASK;

		if (type == PKT_P2P) {
			if (packet->header & TCR_PAYLOAD) { // Has payload
				if ((packet->header & P2P_TYPE_BITS) == P2P_TYPE_SDP) { // SDP?
					p2p_rcv_pkt(packet->key, packet->payload);
					//Don't print packet
					PRINT_DEBUG_PACKET()
				} else { //Unused
					PRINT_PACKET()
				}
			} else { //Unused
				PRINT_PACKET()
			}
		} else if (type == PKT_NN) {
			if (packet->header & TCR_PAYLOAD) {	// Has payload
				if (packet->key & 1) {	// Reply to peek
					PRINT_PACKET()
				} else {	// SNP protocol
					if ((packet->header & PKT_NND_P) == PKT_NND_P) { //Poke packets
						//Fixed reply, we lie pretending that everything is alright
						send_nn(packet->key + 1);
						//Don't print packet
						PRINT_DEBUG_PACKET()
					} else if (packet->key == NN_SDP_KEY) {	// SDP over SNP
						p2p_rcv_pkt(BIT_31, packet->payload);
						//Don't print packet
						PRINT_DEBUG_PACKET()
					} else {	// Plain SNP
						//TODO:unhandled nn_rcv_pkt(link, pkt.data, pkt.key);
						PRINT_PACKET()
					}
				}
			} else {	// No payload
				if (packet->key & 1) {	// Reply to poke
					PRINT_PACKET()
				} else {	// not used
					if ((packet->header & PKT_NND) == PKT_NND) { //Peek packets
						if (packet->key == SYSCTL_BASE) { //Fixed reply
							send_nn_payload(SYSCTL_BASE + 1, CHIP_ID);
							PRINT_DEBUG_PACKET()
						} else {
							PRINT_PACKET()
						}
					} else {
						PRINT_PACKET()
					}
				}
			}
		} else if (type == PKT_MC) { //Unused
			PRINT_PACKET()
		} else { //Unused
			// FR
			PRINT_PACKET()
		}
		shadowBufferReadPointer = (shadowBufferReadPointer + 1) & PACKET_BUFFER_MASK;
	}
//if this packet is supposed to to be sent through serial port
	if (packets_to_be_printed[packetBufferReadPointer >> 5] & _BIT(packetBufferReadPointer % 32)) {
		uint8_t * p = packets[packetBufferReadPointer].buffer;
		uint32_t packet_size_sending = 5; // header + 4bytes for key
		if (p[0] & TCR_PAYLOAD)
			packet_size_sending += 4;
		if (free_space_for(packet_size_sending + 1)) {
			if (packet_size_sending == 5) {
				pushByte(&tx_buffer, PACKET_CMD);
				pushByte(&tx_buffer, *(p++));
				for (uint32_t i = 0; i < sizeof(packet->key); ++i) {
					pushByte(&tx_buffer, *(p++));
				}
			} else {
				pushByte(&tx_buffer, PACKET_PAYLOAD_CMD);
				pushByte(&tx_buffer, *(p++)); //Header
				for (uint32_t i = 0; i < sizeof(packet->key); ++i) {
					pushByte(&tx_buffer, *(p++));//key
				}
				for (uint32_t i = 0; i < sizeof(packet->payload); ++i) {
					pushByte(&tx_buffer, *(p++));//payload
				}
			}
			packets_to_be_printed[packetBufferReadPointer >> 5] &= ~_BIT(packetBufferReadPointer % 32);
			packetBufferReadPointer = (packetBufferReadPointer + 1) & PACKET_BUFFER_MASK;
		}
	} else {
		packetBufferReadPointer = (packetBufferReadPointer + 1) & PACKET_BUFFER_MASK;
	}
}

static void p2p_rcv_pkt(uint32_t addr, uint32_t data) {
	if (data & P2P_CTRL) {
		uint32_t t = chksum_32(data);

		if (t != 0)
			return;

		uint32_t cmd = data & 0x0f000000;

		if (cmd == P2P_OPEN_REQ)
			p2p_open_req(addr, data);
		else if (cmd == P2P_OPEN_ACK)
			p2p_open_ack(addr, data);
		else if (cmd == P2P_DATA_ACK)
			p2p_data_ack(addr, data);
		else if (cmd == P2P_CLOSE_REQ)
			p2p_close_req(addr, data);
		else if (cmd == P2P_CLOSE_ACK)
			p2p_close_ack(addr, data);
	} else {
		p2p_data_pkt(addr, data);
	}
}
