/*
 * board.c
 *
 *  Created on: Jul 18, 2014
 *      Author: raraujo
 */
#include "chip.h"
#include "spinnaker_io_pins.h"
#include "extra_pins.h"
#include "uart.h"

static const PINMUX_GRP_T pinmuxing[] = {
		{ LED0_PORT, LED0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LED1_PORT, LED1_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_0_PORT, LINK0_IN_0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_1_PORT, LINK0_IN_1_PIN,SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_2_PORT, LINK0_IN_2_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_3_PORT, LINK0_IN_3_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_4_PORT, LINK0_IN_4_PIN, SCU_PINIO_FAST| SCU_MODE_FUNC0 },
		{ LINK0_IN_5_PORT, LINK0_IN_5_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_6_PORT, LINK0_IN_6_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_IN_A_PORT, LINK0_IN_A_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_0_PORT, LINK0_OUT_0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_1_PORT, LINK0_OUT_1_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_2_PORT, LINK0_OUT_2_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_3_PORT, LINK0_OUT_3_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_4_PORT, LINK0_OUT_4_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_5_PORT, LINK0_OUT_5_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_6_PORT, LINK0_OUT_6_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ LINK0_OUT_A_PORT, LINK0_OUT_A_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ SPINNAKER_RESET_PORT, SPINNAKER_RESET_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ SPINNAKER_POR_PORT, SPINNAKER_POR_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		{ SPINNAKER_CLOCK_PORT, SPINNAKER_CLOCK_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC1 },
		{ TXD0_PORT, TXD0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC1 },
		{ RXD0_PORT, RXD0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC1 },
		{ RTS0_PORT, RTS0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC4 },
		{ CTS0_PORT, CTS0_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC4 },
		{ SAVE_BOOT_PORT, SAVE_BOOT_PIN, SCU_PINIO_FAST | SCU_MODE_FUNC0 },
		};


static void disablePeripherals() {
	/**
	 * The order is relevant.
	 * The peripherals' clocks is disabled first, then the base clocks and
	 * finally the PLLs.
	 * Inversion of this order may lead to lockup.
	 */
	Chip_Clock_Disable(CLK_APB3_CAN0);
	Chip_Clock_Disable(CLK_APB1_I2S);
	Chip_Clock_Disable(CLK_APB1_CAN1);
	Chip_Clock_Disable(CLK_MX_SPIFI);
	Chip_Clock_Disable(CLK_MX_LCD);
	Chip_Clock_Disable(CLK_MX_ETHERNET);
	Chip_Clock_Disable(CLK_MX_USB0);
	Chip_Clock_Disable(CLK_MX_EMC);
	Chip_Clock_Disable(CLK_MX_SDIO);
	Chip_Clock_Disable(CLK_MX_DMA);
	Chip_Clock_Disable(CLK_MX_SCT);
	Chip_Clock_Disable(CLK_MX_USB1);
	Chip_Clock_Disable(CLK_MX_EMC_DIV);
	//Chip_Clock_Disable(CLK_MX_FLASHB);
	Chip_Clock_Disable(CLK_MX_ADCHS);
	Chip_Clock_Disable(CLK_MX_EEPROM);
	Chip_Clock_Disable(CLK_MX_WWDT);
	Chip_Clock_Disable(CLK_MX_SSP0);
	Chip_Clock_Disable(CLK_MX_TIMER0);
	Chip_Clock_Disable(CLK_MX_UART2);
	Chip_Clock_Disable(CLK_MX_UART3);
	Chip_Clock_Disable(CLK_MX_TIMER2);
	Chip_Clock_Disable(CLK_MX_TIMER3);
	Chip_Clock_Disable(CLK_MX_SSP1);
	Chip_Clock_Disable(CLK_MX_QEI);
	Chip_Clock_Disable(CLK_PERIPH_SGPIO);
	Chip_Clock_DisableBaseClock(CLK_BASE_USB0);
	Chip_Clock_DisableBaseClock(CLK_BASE_USB1);
	Chip_Clock_DisableBaseClock(CLK_BASE_SPIFI);
	Chip_Clock_DisableBaseClock(CLK_BASE_PHY_RX);
	Chip_Clock_DisableBaseClock(CLK_BASE_LCD);
	Chip_Clock_DisableBaseClock(CLK_BASE_ADCHS);
	Chip_Clock_DisableBaseClock(CLK_BASE_SDIO);
	Chip_Clock_DisableBaseClock(CLK_BASE_UART2);
	Chip_Clock_DisableBaseClock(CLK_BASE_UART3);
	Chip_Clock_DisableBaseClock(CLK_BASE_OUT);
	Chip_Clock_DisableBaseClock(CLK_BASE_APLL);
	Chip_Clock_DisableBaseClock(CLK_BASE_CGU_OUT0);
	Chip_Clock_DisableBaseClock(CLK_BASE_CGU_OUT1);
	Chip_Clock_DisablePLL(CGU_USB_PLL);
	Chip_Clock_DisablePLL(CGU_AUDIO_PLL);
}

#define CLOCK_FREQ (180000000)
void Board_SystemInit() {
	/* Initial internal clocking */
	Chip_SetupCoreClock(CLKIN_IRC, CLOCK_FREQ, true);
	disablePeripherals();
	Chip_RIT_Init(LPC_RITIMER);
	Chip_SCU_SetPinMuxing(pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));

}
