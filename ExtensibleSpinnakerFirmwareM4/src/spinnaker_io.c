/*
 * spinnaker_io.c
 *
 *  Created on: Aug 7, 2013
 *      Author: raraujo
 */
#include <cr_section_macros.h>
#include "chip.h"
#include "spinnaker_io.h"
#include "spinnaker_io_pins.h"
#include "extra_pins.h"
#include "uart.h"
#include <string.h>
//Declaration of static functions
static void send_byte(uint8_t byte);
static void send_word(uint32_t word);
static void send_eop();
static uint8_t parity_word(uint32_t word); //1 if an odd number of bits set, 0 otherwise)
static uint8_t parity_byte(uint8_t byte); //1 if an odd number of bits set, 0 otherwise)

//Source:http://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
static uint8_t parity_word(uint32_t word) {
	word ^= word >> 1;
	word ^= word >> 2;
	word = (word & 0x11111111U) * 0x11111111U;
	return (word >> 28) & 0x01;
}
static uint8_t parity_byte(uint8_t byte) {
	byte ^= byte >> 16;
	byte ^= byte >> 8;
	byte ^= byte >> 4;
	byte &= 0xf;
	return (0x6996 >> byte) & 1;
}

/*Compute 4-bit 1's complement checksum of 32 bits. Sum is returned in
 top 4 bits of result & other bits are zero. */
uint32_t chksum_32(uint32_t a) {
	register uint32_t chk __asm__("r0"); //make sure that the checksum is returned correctly
	__asm__ __volatile__(
			"adds    r0, r0, r0, lsl #16\n\t" //     ; s = s + s << 16;
			"it		 cs\n\t"
			"addcs   r0, r0, #0x00010000\n\t"//     ; Add back carry
			"bic     r0, r0, #0x0000ff00 \n\t"//    ; Ensure no carry in
			"adds    r0, r0, r0, lsl #8 \n\t"//     ; s = s + s << 8
			"it		 cs\n\t"
			"addcs   r0, r0, #0x01000000 \n\t"//    ; Add back carry
			"bic     r0, r0, #0x00ff0000 \n\t"//    ; Ensure no carry in
			"adds    r0, r0, r0, lsl #4  \n\t"//    ; s = s + s << 4
			"it		 cs\n\t"
			"addcs   r0, r0, #0x10000000\n\t"//     ; Add back carry
			"and     r0, r0, #0xf0000000 \n\t"//    ; Isolate checksum
			"eor     r0, r0, #0xf0000000 \n\t"//    ; Complement top 4
	);
	return chk;
}

/*Compute 4-bit 1's complement checksum of 64 bits. Sum is returned in
 top 4 bits of result & other bits are zero. */
uint32_t chksum_64(uint32_t key, uint32_t data) {
	register uint32_t key2 __asm__("r0"); //make sure that the checksum is returned correctly
	__asm__ __volatile__(
			"adds    r0, r0, r1\n\t"             // ; s = a + b
			"it		 cs\n\t"
			"addcs   r0, r0, #1 \n\t"//             ; Add back carry
			"adds    r0, r0, r0, lsl #16\n\t"//     ; s = s + s << 16;
			"it		 cs\n\t"
			"addcs   r0, r0, #0x00010000\n\t"//     ; Add back carry
			"bic     r0, r0, #0x0000ff00 \n\t"//    ; Ensure no carry in
			"adds    r0, r0, r0, lsl #8 \n\t"//     ; s = s + s << 8
			"it		 cs\n\t"
			"addcs   r0, r0, #0x01000000 \n\t"//    ; Add back carry
			"bic     r0, r0, #0x00ff0000 \n\t"//    ; Ensure no carry in
			"adds    r0, r0, r0, lsl #4  \n\t"//    ; s = s + s << 4
			"it		 cs\n\t"
			"addcs   r0, r0, #0x10000000\n\t"//     ; Add back carry
			"and     r0, r0, #0xf0000000 \n\t"//    ; Isolate checksum
			"eor     r0, r0, #0xf0000000 \n\t"//    ; Complement top 4
	);
	return key2;
}
__NOINIT(RAM2) union spin_packet_byte_accessible packets[PACKET_BUFFER_SIZE];
__NOINIT(RAM6) volatile uint32_t packetBufferWritePointer = 0;
__NOINIT(RAM7) volatile uint32_t packetBufferReadPointer = 0;
uint32_t p2p_source_address = 0;
/*
 * This variable holds the state
 */
static uint32_t output_link; //Current status of the output ACK

void send_packet(uint8_t header, uint32_t address) {
	uint8_t header_parity, address_parity; //parity calculations
	header_parity = parity_byte(header);
	address_parity = parity_word(address);
	header |= ~(header_parity ^ address_parity) & 0x1; //Odd parity
	send_byte(header);
	send_word(address);
	send_eop();
}
#ifdef DEBUG
volatile uint32_t debug_p2p = 0;
#endif
void send_packet_payload(uint8_t header, uint32_t address, uint32_t payload) {
	uint8_t header_parity, address_parity, payload_parity; //parity calculations
	header_parity = parity_byte(header);
	address_parity = parity_word(address);
	payload_parity = payload == 0 ? 0 : parity_word(payload);
	header |= ~(header_parity ^ address_parity ^ payload_parity) & 0x1; //Odd parity
	send_byte(header);
	send_word(address);
	send_word(payload);
	send_eop();
#ifdef DEBUG
	if (debug_p2p) {
		if (free_space_for(10)) {
			pushByte(&tx_buffer, 'S');
			pushByte(&tx_buffer, header);
			pushByte(&tx_buffer, address & 255);
			address >>= 8;
			pushByte(&tx_buffer, address & 255);
			address >>= 8;
			pushByte(&tx_buffer, address & 255);
			address >>= 8;
			pushByte(&tx_buffer, address & 255);
			pushByte(&tx_buffer, payload & 255);
			payload >>= 8;
			pushByte(&tx_buffer, payload & 255);
			payload >>= 8;
			pushByte(&tx_buffer, payload & 255);
			payload >>= 8;
			pushByte(&tx_buffer, payload & 255);
		}
	}
#endif
}

void init_links() {
	packetBufferReadPointer = 0;
	packetBufferWritePointer = 0;
	memset(packets, 0, sizeof(packets));
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, _BIT(LINK0_OUT_0_PIN_GPIO));
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, LINK0_OUT_1_PIN_GPIO);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, LINK0_OUT_2_PIN_GPIO);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, LINK0_OUT_3_PIN_GPIO);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_4_PORT_GPIO, LINK0_OUT_4_PIN_GPIO);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, LINK0_OUT_5_PIN_GPIO);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, LINK0_OUT_6_PIN_GPIO);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, LINK0_IN_A_PORT_GPIO, LINK0_IN_A_PIN_GPIO);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, LINK0_OUT_0_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, LINK0_OUT_1_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, LINK0_OUT_2_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, LINK0_OUT_3_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_4_PORT_GPIO, LINK0_OUT_4_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, LINK0_OUT_5_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, LINK0_OUT_6_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_OUT_A_PORT_GPIO, LINK0_OUT_A_PIN_GPIO);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_0_PORT_GPIO, LINK0_IN_0_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_1_PORT_GPIO, LINK0_IN_1_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_2_PORT_GPIO, LINK0_IN_2_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_3_PORT_GPIO, LINK0_IN_3_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_4_PORT_GPIO, LINK0_IN_4_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_5_PORT_GPIO, LINK0_IN_5_PIN_GPIO);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, LINK0_IN_6_PORT_GPIO, LINK0_IN_6_PIN_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, LINK0_IN_A_PORT_GPIO, LINK0_IN_A_PIN_GPIO);
	output_link = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LINK0_OUT_A_PORT_GPIO, LINK0_OUT_A_PIN_GPIO);
}

static void wait_for_ack() {
//Wait for ACK

	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO);
	if (output_link == 0) {
		do {
			output_link = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LINK0_OUT_A_PORT_GPIO, LINK0_OUT_A_PIN_GPIO);
		} while (output_link == 0);
	} else {
		do {
			output_link = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, LINK0_OUT_A_PORT_GPIO, LINK0_OUT_A_PIN_GPIO);
		} while (output_link != 0);
	}
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, LED1_PORT_GPIO, LED1_PIN_GPIO);
}

void nibble0() {
	//The compiler should optimized this check away
	if (LINK0_OUT_0_PORT_GPIO == LINK0_OUT_4_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO,
		_BIT(LINK0_OUT_0_PIN_GPIO) | _BIT(LINK0_OUT_4_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, _BIT(LINK0_OUT_0_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_4_PORT_GPIO, _BIT(LINK0_OUT_4_PIN_GPIO));
	}
}
void nibble1() {
	//The compiler should optimized this check away
	if (LINK0_OUT_1_PORT_GPIO == LINK0_OUT_4_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO,
		_BIT(LINK0_OUT_1_PIN_GPIO) | _BIT(LINK0_OUT_4_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, _BIT(LINK0_OUT_1_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_4_PORT_GPIO, _BIT(LINK0_OUT_4_PIN_GPIO));
	}
}
void nibble2() {
	//The compiler should optimized this check away
	if (LINK0_OUT_2_PORT_GPIO == LINK0_OUT_4_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO,
		_BIT(LINK0_OUT_2_PIN_GPIO) | _BIT(LINK0_OUT_4_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, _BIT(LINK0_OUT_2_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_4_PORT_GPIO, _BIT(LINK0_OUT_4_PIN_GPIO));
	}
}
void nibble3() {
	//The compiler should optimized this check away
	if (LINK0_OUT_3_PORT_GPIO == LINK0_OUT_4_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO,
		_BIT(LINK0_OUT_3_PIN_GPIO) | _BIT(LINK0_OUT_4_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, _BIT(LINK0_OUT_3_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_4_PORT_GPIO, _BIT(LINK0_OUT_4_PIN_GPIO));
	}
}
void nibble4() {
	//The compiler should optimized this check away
	if (LINK0_OUT_0_PORT_GPIO == LINK0_OUT_5_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO,
		_BIT(LINK0_OUT_0_PIN_GPIO) | _BIT(LINK0_OUT_5_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, _BIT(LINK0_OUT_0_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, _BIT(LINK0_OUT_5_PIN_GPIO));
	}
}
void nibble5() {
	//The compiler should optimized this check away
	if (LINK0_OUT_1_PORT_GPIO == LINK0_OUT_5_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO,
		_BIT(LINK0_OUT_1_PIN_GPIO) | _BIT(LINK0_OUT_5_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, _BIT(LINK0_OUT_1_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, _BIT(LINK0_OUT_5_PIN_GPIO));
	}
}
void nibble6() {
	//The compiler should optimized this check away
	if (LINK0_OUT_2_PORT_GPIO == LINK0_OUT_5_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO,
		_BIT(LINK0_OUT_2_PIN_GPIO) | _BIT(LINK0_OUT_5_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, _BIT(LINK0_OUT_2_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, _BIT(LINK0_OUT_5_PIN_GPIO));
	}
}
void nibble7() {
	//The compiler should optimized this check away
	if (LINK0_OUT_3_PORT_GPIO == LINK0_OUT_5_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO,
		_BIT(LINK0_OUT_3_PIN_GPIO) | _BIT(LINK0_OUT_5_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, _BIT(LINK0_OUT_3_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, _BIT(LINK0_OUT_5_PIN_GPIO));
	}
}
void nibble8() {
	//The compiler should optimized this check away
	if (LINK0_OUT_0_PORT_GPIO == LINK0_OUT_6_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO,
		_BIT(LINK0_OUT_0_PIN_GPIO) | _BIT(LINK0_OUT_6_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, _BIT(LINK0_OUT_0_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, _BIT(LINK0_OUT_6_PIN_GPIO));
	}
}
void nibble9() {
	//The compiler should optimized this check away
	if (LINK0_OUT_1_PORT_GPIO == LINK0_OUT_6_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO,
		_BIT(LINK0_OUT_1_PIN_GPIO) | _BIT(LINK0_OUT_6_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, _BIT(LINK0_OUT_1_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, _BIT(LINK0_OUT_6_PIN_GPIO));
	}
}
void nibble10() {
	//The compiler should optimized this check away
	if (LINK0_OUT_2_PORT_GPIO == LINK0_OUT_6_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO,
		_BIT(LINK0_OUT_2_PIN_GPIO) | _BIT(LINK0_OUT_6_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, _BIT(LINK0_OUT_2_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, _BIT(LINK0_OUT_6_PIN_GPIO));
	}
}
void nibble11() {
	//The compiler should optimized this check away
	if (LINK0_OUT_3_PORT_GPIO == LINK0_OUT_6_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO,
		_BIT(LINK0_OUT_3_PIN_GPIO) | _BIT(LINK0_OUT_6_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, _BIT(LINK0_OUT_3_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, _BIT(LINK0_OUT_6_PIN_GPIO));
	}
}
void nibble12() {
	//The compiler should optimized this check away
	if (LINK0_OUT_0_PORT_GPIO == LINK0_OUT_1_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO,
		_BIT(LINK0_OUT_0_PIN_GPIO) | _BIT(LINK0_OUT_1_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, _BIT(LINK0_OUT_0_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, _BIT(LINK0_OUT_1_PIN_GPIO));
	}
}
void nibble13() {
	//The compiler should optimized this check away
	if (LINK0_OUT_1_PORT_GPIO == LINK0_OUT_2_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO,
		_BIT(LINK0_OUT_1_PIN_GPIO) | _BIT(LINK0_OUT_2_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_1_PORT_GPIO, _BIT(LINK0_OUT_1_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, _BIT(LINK0_OUT_2_PIN_GPIO));
	}
}
void nibble14() {
	//The compiler should optimized this check away
	if (LINK0_OUT_2_PORT_GPIO == LINK0_OUT_3_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO,
		_BIT(LINK0_OUT_2_PIN_GPIO) | _BIT(LINK0_OUT_3_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_2_PORT_GPIO, _BIT(LINK0_OUT_2_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, _BIT(LINK0_OUT_3_PIN_GPIO));
	}
}
void nibble15() {
	//The compiler should optimized this check away
	if (LINK0_OUT_0_PORT_GPIO == LINK0_OUT_3_PORT_GPIO) {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO,
		_BIT(LINK0_OUT_0_PIN_GPIO) | _BIT(LINK0_OUT_3_PIN_GPIO));
	} else {
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_0_PORT_GPIO, _BIT(LINK0_OUT_0_PIN_GPIO));
		Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_3_PORT_GPIO, _BIT(LINK0_OUT_3_PIN_GPIO));
	}
}

void (*send_nibble[16])() = {nibble0, nibble1, nibble2, nibble3, nibble4,
	nibble5, nibble6, nibble7, nibble8, nibble9, nibble10, nibble11,
	nibble12, nibble13, nibble14, nibble15 };

	static void send_byte(uint8_t byte) {
		uint8_t first_nibble = byte & 0x0F;
		uint8_t second_nibble = byte >> 4;
		(*send_nibble[first_nibble])();
		//wait_for_ack(); SpiNNaker is way faster than us
		(*send_nibble[second_nibble])();
		//wait_for_ack(); SpiNNaker is way faster than us
	}

	static void send_word(uint32_t word) {
//first byte
		send_byte((uint8_t) word);
		word >>= 8;
//second byte
		send_byte((uint8_t) word);
		word >>= 8;
//third byte
		send_byte((uint8_t) word);
		word >>= 8;
//forth byte
		send_byte((uint8_t) word);
	}

	static void send_eop() {
//The compiler should optimized this check away
		if (LINK0_OUT_5_PORT_GPIO == LINK0_OUT_6_PORT_GPIO) {
			Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO,
			_BIT(LINK0_OUT_5_PIN_GPIO) | _BIT(LINK0_OUT_6_PIN_GPIO));
		} else {
			Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_5_PORT_GPIO, _BIT(LINK0_OUT_5_PIN_GPIO));
			Chip_GPIO_SetPortToggle(LPC_GPIO_PORT, LINK0_OUT_6_PORT_GPIO, _BIT(LINK0_OUT_6_PIN_GPIO));
		}
		wait_for_ack();
	}

