/*
 * command_protocol.h
 *
 *  Created on: Oct 23, 2013
 *      Author: raraujo
 */

#ifndef COMMAND_PROTOCOL_H_
#define COMMAND_PROTOCOL_H_


//CMDs supported to the application sent by the uC
#define PACKET_CMD					('p')
#define PACKET_PAYLOAD_CMD			('P')
//SDP_CMD is also used here

//CMDs supported to the uC sent by the application
#define BOOT_CMD					('b')
#define P2P_ADDRESS_CMD				('a')
#define SDP_CMD						('s')
#define PEEK_CMD					('e')
#define POKE_CMD					('E')
#define P2P_CMD						('p')
#define P2P_PAYLOAD_CMD				('P')
#define FIXED_ROUTE_CMD				('f')
#define FIXED_ROUTE_PAYLOAD_CMD		('F')
#define NN_CMD						('n')
#define NN_PAYLOAD_CMD				('N')
#define MULTICAST_CMD				('m')
#define MULTICAST_PAYLOAD_CMD		('M')
#define CMD_END						('\n')


#endif /* COMMAND_PROTOCOL_H_ */
