/****a* spinn_sdp.h/spinn_sdp_header
*
* SUMMARY
*  SpiNNaker Datagram Protocol (SDP) main header file
*
* AUTHOR
*  Steve Temple - temples@cs.man.ac.uk
*
* DETAILS
*  Created on       : 03 May 2011
*  Version          : $Revision: 1194 $
*  Last modified on : $Date: 2011-06-25 13:10:28 +0100 (Sat, 25 Jun 2011) $
*  Last modified by : $Author: plana $
*  $Id: spinn_api.h 1194 2011-06-25 12:10:28Z plana $
*  $HeadURL: file:///home/amulinks/spinnaker/svn/spinn_api/trunk/src/spinn_api.h $
*
* COPYRIGHT
*  Copyright (c) The University of Manchester, 2011. All rights reserved.
*  SpiNNaker Project
*  Advanced Processor Technologies Group
*  School of Computer Science
*
*******/

#ifndef __SPINN_SDP_H__
#define __SPINN_SDP_H__


#define PORT_ETH 		255
#define PORT_SHIFT		5
#define PORT_MASK		7
#define CPU_MASK		31

#define SDP_BUF_SIZE 		(BUF_SIZE)


// ------------------------------------------------------------------------
// SDP type definitions
// ------------------------------------------------------------------------
// Note that the length field is the number of bytes following
// the checksum. It will be a minimum of 8 as the SDP header
// should always be present.

typedef struct sdp_msg		// SDP message (=288 bytes)
{
  uint16_t length;		// length
  uint16_t checksum;		// checksum (if used)

  // sdp_hdr_t

  uint8_t flags;			// SDP flag byte
  uint8_t tag;			// SDP IPtag
  uint8_t dest_port_cpu;		// SDP destination port/CPU
  uint8_t srce_port_cpu;		// SDP source port/CPU
  uint16_t dest_addr;		// SDP destination address
  uint16_t srce_addr;		// SDP source address

  // cmd_hdr_t (optional)

  uint16_t cmd_rc;		// Command/Return Code
  uint16_t seq;			// Sequence number
  uint32_t arg1;			// Arg 1
  uint32_t arg2;			// Arg 2
  uint32_t arg3;			// Arg 3

  // user data (optional)

  uint8_t data[SDP_BUF_SIZE];	// User data (256 bytes)

  uint32_t _PAD;			// Private padding
} sdp_msg_t;


#endif /* __SPINN_SDP_H__ */
