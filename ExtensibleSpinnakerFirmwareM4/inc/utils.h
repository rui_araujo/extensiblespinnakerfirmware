/*
 * utils.h
 *
 *  Created on: Apr 24, 2014
 *      Author: raraujo
 */

#ifndef UTILS_H_
#define UTILS_H_
#include "chip.h"
#include <stdint.h>

extern RTC_TIME_T buildTime;


/**
 * Busy looping using the RI timer in the LPC4337
 * @param[in] us microseconds for the delay
 */
void timerDelayUs(uint32_t us);
/**
 * Busy looping using the RI timer in the LPC4337
 * It uses
 * @param[in] ms milliseconds to delays
 */
void timerDelayMs(uint32_t ms);

/**
 * It reset the entire LPC4337 chip
 */
void resetDevice();

/**
 * It enters reprogramming mode, i.e., ISP mode.
 * It mimics the condition of the LP4337 after reset in ISP mode.
 */
void enterReprogrammingMode();


#endif /* UTILS_H_ */
