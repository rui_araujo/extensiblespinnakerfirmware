/*
 * spinnaker_io.h
 *
 *  Created on: 6 de Ago de 2013
 *      Author: ruka
 */

#ifndef SPINNAKER_IO_H_
#define SPINNAKER_IO_H_
#include <stdint.h>
#include "spinnaker.h"

/**
 * We have to use uint8_t as the basic unit because using int will lead to padding and other problem
 */
struct spin_packet {
	uint8_t header;
	uint32_t key; // actual use depends on packet type
	uint32_t payload; //optional
} __attribute__ ((__packed__));

//This union eases the reception of the packet which checking where we are
union spin_packet_byte_accessible {
	uint8_t buffer[sizeof(struct spin_packet)];
	struct spin_packet packet;
};

#define BUILD_BUG_ON(condition) static inline void __build_assert(void){((void)sizeof(char[1 - 2*!!(condition)]));}

//We rely on the struct size being packed
BUILD_BUG_ON(sizeof(union spin_packet_byte_accessible)!=9);

#define RAM_SIZE					(0x9000)
#define PACKET_BUFFER_SIZE		  	((uint32_t)(RAM_SIZE/sizeof(union spin_packet_byte_accessible))) //0x1000
#define PACKET_BUFFER_MASK		  	(PACKET_BUFFER_SIZE - 1)

extern union spin_packet_byte_accessible packets[PACKET_BUFFER_SIZE ];
extern volatile uint32_t packetBufferWritePointer;
extern volatile uint32_t packetBufferReadPointer;
#ifdef DEBUG
extern volatile uint32_t debug_p2p;
#endif

extern uint32_t p2p_source_address;

#define send_peek(address) send_packet(PKT_NND, address)
#define send_poke(address, payload) send_packet_payload(PKT_NND_P, address, payload)
#define send_mc(address) send_packet(PKT_MC, address)
#define send_mc_payload(address, payload) send_packet_payload(PKT_MC_P, address, payload)
#define send_p2p(address) send_packet(PKT_P2P, p2p_source_address | (address&0xffff))
#define send_p2p_payload(address, payload) send_packet_payload(PKT_P2P_P, p2p_source_address | (address&0xffff), payload)
#define send_nn(address) send_packet(PKT_NN, address)
#define send_nn_payload(address, payload) send_packet_payload(PKT_NN_P, address, payload)
#define send_fr(address) send_packet(PKT_FR, address)
#define send_fr_payload(address, payload) send_packet_payload(PKT_FR_P, address, payload)
extern void send_packet(uint8_t header, uint32_t address);
extern void send_packet_payload(uint8_t header, uint32_t address,
		uint32_t payload);
extern void parse_nibble();
extern void init_links(void);
extern uint32_t chksum_32(uint32_t a);
extern uint32_t chksum_64(uint32_t key, uint32_t data);

#endif /* SPINNAKER_IO_H_ */
