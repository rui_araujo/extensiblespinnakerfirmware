/*
 * spinnaker_noc.h
 *
 *  Created on: Oct 23, 2013
 *      Author: raraujo
 */

#ifndef SPINNAKER_NOC_H_
#define SPINNAKER_NOC_H_
#include <stdint.h>

extern void init_processing();
extern void process_packet();

#endif /* SPINNAKER_NOC_H_ */
