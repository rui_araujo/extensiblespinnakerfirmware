/*
 * spinnaker_p2p.h
 *
 *  Created on: 24 de Nov de 2013
 *      Author: ruka
 */

#ifndef SPINNAKER_P2P_H_
#define SPINNAKER_P2P_H_
#include <stdint.h>
#include "spinnaker_sdp.h"

typedef struct rx_desc {
	uint8_t state;
	uint8_t seq_len;
	uint8_t done;
	uint8_t tid;

	uint8_t rid;
	uint8_t tcount;
	uint8_t ctrl;
	uint8_t phase;

	uint16_t mask;
	uint16_t new_mask;

	uint32_t srce;

	uint8_t *base;
	uint8_t *limit;
	sdp_msg_t msg;

} rx_desc_t;

typedef struct tx_desc {
	uint8_t seq_len;
	uint8_t seq;
	uint8_t odd;
	uint8_t done;

	uint8_t state;
	uint8_t rid;
	uint8_t tid;
	uint8_t tcount;

	uint8_t rc;
	uint8_t phase;
	uint8_t ack;

	uint16_t mask;

	uint32_t dest;
	uint32_t len;

	uint8_t *base;
	uint8_t *limit;

} tx_desc_t;

#define P2P_NUM_STR 		4	// Number of streams
#define TX_IDLE 0
#define TX_OPEN_REQ 1
#define TX_OPEN 2
#define TX_OPEN_ACK 3
#define TX_RETRY 4
#define TX_CLOSE 5

#define RX_IDLE 0
#define RX_OPEN 1
#define RX_CLOSE_REQ 2
#define RX_SENDING 3

extern volatile uint32_t tx_timeout;
extern volatile uint32_t rx_timeout;

extern sdp_msg_t tx_sdp;
extern sdp_msg_t rx_sdp;

extern tx_desc_t tx_desc;
extern rx_desc_t rx_desc_table[P2P_NUM_STR];

extern void p2p_data_pkt(uint32_t addr, uint32_t data);
extern void p2p_rx_check_timeout();
extern void p2p_start_send_msg(sdp_msg_t *msg);
extern void p2p_continue_send_msg();
extern void p2p_data_ack(uint32_t srce, uint32_t data);
extern void p2p_close_req(uint32_t srce, uint32_t data);
extern void p2p_close_ack(uint32_t srce, uint32_t data);
extern void p2p_open_ack(uint32_t srce, uint32_t data);
extern void p2p_open_req(uint32_t addr, uint32_t data);
extern void p2p_close_timeout(rx_desc_t *desc);
extern void p2p_data_timeout(rx_desc_t *desc);

#endif /* SPINNAKER_P2P_H_ */
