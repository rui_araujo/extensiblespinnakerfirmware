#ifndef __UART_H 
#define __UART_H
#include <stdint.h>

#define TXD0_PORT                		(2)
#define TXD0_PIN                		(0)

#define RXD0_PORT                		(2)
#define RXD0_PIN                		(1)

#define RTS0_PORT                		(2)
#define RTS0_PIN                		(3)
#define RTS0_GPIO_PORT                	(5)
#define RTS0_GPIO_PIN               	(3)

#define CTS0_PORT						(2)
#define CTS0_PIN						(2)
#define CTS0_GPIO_PORT					(5)
#define CTS0_GPIO_PIN					(2)

#define TX_BUFFER_SIZE_BITS		(13)
#define TX_BUFFER_SIZE			(1<<TX_BUFFER_SIZE_BITS)
#define TX_BUFFER_MASK			(TX_BUFFER_SIZE-1)


/*The system clock rate is set to 180Mhz so the these are good baudrate with low error*/
#define HIGH_SPEED_BAUDRATE			(9000000)
#define MEDIUM_SPEED_BAUDRATE		(6000000)
#define LOW_SPEED_BAUDRATE			(3000000)
#define MEGA_LOW_SPEED_BAUDRATE		(500000)
#define ULTRA_LOW_SPEED_BAUDRATE	(115200)

struct transmist_buffer {
	uint8_t buffer[TX_BUFFER_SIZE];
	uint32_t bufferWritePointer;
	uint32_t bufferReadPointer;
};
//Transmit buffer that will be used on the rest of the system
extern struct transmist_buffer tx_buffer;

static inline void pushByte(struct transmist_buffer * tx_buffer, uint8_t byte) {
	tx_buffer->buffer[tx_buffer->bufferWritePointer] = byte;
	tx_buffer->bufferWritePointer = (tx_buffer->bufferWritePointer + 1) & TX_BUFFER_MASK;
}

#define UART0_READY(); Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,CTS0_GPIO_PORT, CTS0_GPIO_PIN);
#define UART0_BUSY(); Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,CTS0_GPIO_PORT, CTS0_GPIO_PIN);

extern void UARTInit(uint32_t baudrate);

static inline uint32_t free_space() {
	return (tx_buffer.bufferReadPointer - tx_buffer.bufferWritePointer - 1) & TX_BUFFER_MASK;
}

static inline uint32_t free_space_for(uint32_t nbytes) {
	return free_space() >= nbytes;
}
#endif /* end __UART_H */
