/*
 * spinnaker_io_pins.h
 *
 *  Created on: Oct 14, 2013
 *      Author: raraujo
 */

#ifndef SPINNAKER_IO_PINS_H_
#define SPINNAKER_IO_PINS_H_


#define SPINNAKER_RESET_PORT			(1)
#define SPINNAKER_RESET_PORT_GPIO		(0)
#define SPINNAKER_RESET_PIN				(20)
#define SPINNAKER_RESET_PIN_GPIO		(15)

#define SPINNAKER_POR_PORT				(4)
#define SPINNAKER_POR_PORT_GPIO			(2)
#define SPINNAKER_POR_PIN				(1)
#define SPINNAKER_POR_PIN_GPIO			(1)

#define SPINNAKER_CLOCK_PORT			(5)
#define SPINNAKER_CLOCK_PIN				(0)

#define LINK0_IN_0_PORT_GPIO  			(1)
#define LINK0_IN_0_PIN_GPIO  			(4)
#define LINK0_IN_0_PORT  				(1)
#define LINK0_IN_0_PIN	 				(11)

#define LINK0_IN_1_PORT_GPIO  			(1)
#define LINK0_IN_1_PIN_GPIO  			(5)
#define LINK0_IN_1_PORT  				(1)
#define LINK0_IN_1_PIN	 				(12)

#define LINK0_IN_2_PORT_GPIO  			(1)
#define LINK0_IN_2_PIN_GPIO  			(6)
#define LINK0_IN_2_PORT  				(1)
#define LINK0_IN_2_PIN	 				(13)

#define LINK0_IN_3_PORT_GPIO  			(1)
#define LINK0_IN_3_PIN_GPIO  			(7)
#define LINK0_IN_3_PORT  				(1)
#define LINK0_IN_3_PIN	 				(14)

#define LINK0_IN_4_PORT_GPIO  			(1)
#define LINK0_IN_4_PIN_GPIO  			(2)
#define LINK0_IN_4_PORT  				(1)
#define LINK0_IN_4_PIN	 				(9)

#define LINK0_IN_5_PORT_GPIO  			(1)
#define LINK0_IN_5_PIN_GPIO  			(3)
#define LINK0_IN_5_PORT  				(1)
#define LINK0_IN_5_PIN	 				(10)

#define LINK0_IN_6_PORT_GPIO  			(1)
#define LINK0_IN_6_PIN_GPIO  			(1)
#define LINK0_IN_6_PORT  				(1)
#define LINK0_IN_6_PIN	 				(8)

#define LINK0_IN_A_PORT_GPIO  			(0)
#define LINK0_IN_A_PIN_GPIO  			(2)
#define LINK0_IN_A_PORT  				(1)
#define LINK0_IN_A_PIN	 				(15)

#define LINK0_OUT_0_PORT_GPIO  			(1)
#define LINK0_OUT_0_PIN_GPIO  			(0)
#define LINK0_OUT_0_PORT  				(1)
#define LINK0_OUT_0_PIN	 				(7)

#define LINK0_OUT_1_PORT_GPIO  			(1)
#define LINK0_OUT_1_PIN_GPIO  			(9)
#define LINK0_OUT_1_PORT  				(1)
#define LINK0_OUT_1_PIN	 				(6)

#define LINK0_OUT_2_PORT_GPIO  			(0)
#define LINK0_OUT_2_PIN_GPIO  			(10)
#define LINK0_OUT_2_PORT  				(1)
#define LINK0_OUT_2_PIN	 				(3)

#define LINK0_OUT_3_PORT_GPIO  			(0)
#define LINK0_OUT_3_PIN_GPIO  			(4)
#define LINK0_OUT_3_PORT  				(1)
#define LINK0_OUT_3_PIN	 				(0)

#define LINK0_OUT_4_PORT_GPIO  			(1)
#define LINK0_OUT_4_PIN_GPIO  			(8)
#define LINK0_OUT_4_PORT  				(1)
#define LINK0_OUT_4_PIN	 				(5)

#define LINK0_OUT_5_PORT_GPIO  			(0)
#define LINK0_OUT_5_PIN_GPIO  			(11)
#define LINK0_OUT_5_PORT  				(1)
#define LINK0_OUT_5_PIN	 				(4)

#define LINK0_OUT_6_PORT_GPIO  			(0)
#define LINK0_OUT_6_PIN_GPIO  			(9)
#define LINK0_OUT_6_PORT  				(1)
#define LINK0_OUT_6_PIN	 				(2)

#define LINK0_OUT_A_PORT_GPIO  			(0)
#define LINK0_OUT_A_PIN_GPIO  			(8)
#define LINK0_OUT_A_PORT  				(1)
#define LINK0_OUT_A_PIN	 				(1)


#endif /* SPINNAKER_IO_PINS_H_ */
