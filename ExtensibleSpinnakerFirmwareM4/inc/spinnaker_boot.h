/*
 * spinnaker_boot.h
 *
 *  Created on: 6 de Ago de 2013
 *      Author: ruka
 */

#ifndef SPINNAKER_BOOT_H_
#define SPINNAKER_BOOT_H_
#include <stdint.h>


// !! Send 16kb image

#define BLOCK_COUNT		32	// From 1-256, lower numbers of blocks is more efficient
#define WORD_COUNT		128	// From 1-256, BLOCK_COUNT * WORD_COUNT must be < 32kB
#define BYTE_COUNT		(WORD_COUNT * sizeof (uint32_t))

extern void reset_spinnaker(void);

extern void boot_nn (uint32_t *  image);

#endif /* SPINNAKER_BOOT_H_ */
