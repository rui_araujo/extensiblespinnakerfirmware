/*
 * extra_pins.h
 *
 *  Created on: 5 de Ago de 2013
 *      Author: ruka
 */

#ifndef EXTRA_PINS_H_
#define EXTRA_PINS_H_


/* The bit of port 0 that the LPCXpresso LPC43xx LED0 is connected. */
#define LED0_PORT_GPIO  			(0)
#define LED0_PIN_GPIO  				(0)
#define LED0_PORT  					(0)
#define LED0_PIN	 				(0)
/* The bit of port 0 that the LPCXpresso LPC43xx LED1 is connected. */
#define LED1_PORT_GPIO  			(0)
#define LED1_PIN_GPIO  				(1)
#define LED1_PORT  					(0)
#define LED1_PIN	 				(1)


/* Toggle to set whether the boot image should be saved. */
#define SAVE_BOOT_PORT_GPIO  		(3)
#define SAVE_BOOT_PIN_GPIO  		(9)
#define SAVE_BOOT_PORT  			(7)
#define SAVE_BOOT_PIN	 			(1)

#endif /* EXTRA_PINS_H_ */
