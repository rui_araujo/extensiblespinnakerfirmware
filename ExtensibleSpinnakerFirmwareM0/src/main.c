/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */
#include "chip.h"
#include "extra_pins.h"
#include "spinnaker_io.h"
#include <cr_section_macros.h>
#include "spinnaker_io_pins.h"


#define EOP (0b0000101)
#define INVALID_VALUE  (0xFF)

//#define M0_SLAVE_PAUSE_AT_MAIN

#if defined (M0_SLAVE_PAUSE_AT_MAIN)
volatile unsigned int pause_at_main = 0;
#endif


static uint8_t nibble_lookup_table[128]; //7bit possibilities.

//These variable are in well known place in memory
__NOINIT(RAM2) union spin_packet_byte_accessible packets[PACKET_BUFFER_SIZE];
__NOINIT(RAM6) volatile uint32_t packetBufferWritePointer = 0;
__NOINIT(RAM7) volatile uint32_t packetBufferReadPointer = 0;

static volatile union spin_packet_byte_accessible * packet;

static uint32_t nibbleIn = INVALID_VALUE; //last nibble received
static uint32_t last_input_state; //last input detected
static uint32_t current_input_state; //current input detected
static uint32_t diff;

static inline void parseNibble() {
	diff = current_input_state ^ last_input_state;
	nibbleIn = nibble_lookup_table[diff];
	last_input_state = current_input_state;
	LPC_GPIO_PORT->NOT[LINK0_IN_A_PORT_GPIO] = _BIT(LINK0_IN_A_PIN_GPIO);
}

int main(void) {
#if defined (M0_SLAVE_PAUSE_AT_MAIN)
	// Pause execution until debugger attaches and modifies variable
	while (pause_at_main == 0) {}
#endif

	packet = &packets[0];

	for (uint32_t i = 0; i < sizeof(nibble_lookup_table); ++i) {
		if (i == 0b0001010) //10
			nibble_lookup_table[i] = 0;
		else if (i == 0b0010010) //18
			nibble_lookup_table[i] = 1;
		else if (i == 0b0100010) //34
			nibble_lookup_table[i] = 2;
		else if (i == 0b1000010) //66
			nibble_lookup_table[i] = 3;
		else if (i == 0b0001100) //12
			nibble_lookup_table[i] = 4;
		else if (i == 0b0010100) //20
			nibble_lookup_table[i] = 5;
		else if (i == 0b0100100) //36
			nibble_lookup_table[i] = 6;
		else if (i == 0b1000100) //68
			nibble_lookup_table[i] = 7;
		else if (i == 0b0001001) //9
			nibble_lookup_table[i] = 8;
		else if (i == 0b0010001) //17
			nibble_lookup_table[i] = 9;
		else if (i == 0b0100001) //33
			nibble_lookup_table[i] = 10;
		else if (i == 0b1000001) //65
			nibble_lookup_table[i] = 11;
		else if (i == 0b0011000) //24
			nibble_lookup_table[i] = 12;
		else if (i == 0b0110000) //48
			nibble_lookup_table[i] = 13;
		else if (i == 0b1100000) //96
			nibble_lookup_table[i] = 14;
		else if (i == 0b1001000) //72
			nibble_lookup_table[i] = 15;
		else
			//if (i == 0b0000101) //5
			nibble_lookup_table[i] = INVALID_VALUE; //EOP
	}

//Reading input
	current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1) & (uint32_t) 0x007F;
	last_input_state = current_input_state;

	CHECK_FOR_SPACE:

	if (((packetBufferWritePointer + 1) & PACKET_BUFFER_MASK)
			== packetBufferReadPointer) {
		goto CHECK_FOR_SPACE;
	}
	NEW_PACKET: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NEW_PACKET;
	nibbleIn = nibble_lookup_table[current_input_state ^ last_input_state];
	last_input_state = current_input_state;
	LPC_GPIO_PORT->NOT[LINK0_IN_A_PORT_GPIO] = _BIT(LINK0_IN_A_PIN_GPIO);
	if (nibbleIn == INVALID_VALUE) //It's the first nibble so EOP or glitch leads to the same result
		goto NEW_PACKET;
	packet->buffer[0] = nibbleIn;
	NIBBLE_0: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_0;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_0;
		// Glitch protection
	}
	packet->buffer[0] |= nibbleIn << 4;
	NIBBLE_1: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_1;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_1;
		// Glitch protection
	}
	packet->buffer[1] = nibbleIn;
	NIBBLE_2: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_2;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_2;
		// Glitch protection
	}
	packet->buffer[1] |= nibbleIn << 4;
	NIBBLE_3: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_3;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_3;
		// Glitch protection
	}
	packet->buffer[2] = nibbleIn;
	NIBBLE_4: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_4;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_4;
		// Glitch protection
	}
	packet->buffer[2] |= nibbleIn << 4;
	NIBBLE_5: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_5;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_5;
		// Glitch protection
	}
	packet->buffer[3] = nibbleIn;
	NIBBLE_6: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_6;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_6;
		// Glitch protection
	}
	packet->buffer[3] |= nibbleIn << 4;
	NIBBLE_7: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_7;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_7;
		// Glitch protection
	}
	packet->buffer[4] = nibbleIn;
	NIBBLE_8: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_8;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_8;
		// Glitch protection
	}
	packet->buffer[4] |= nibbleIn << 4;
	NIBBLE_9: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_9;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) { //Read an EOP
		if (diff == EOP) {
			packetBufferWritePointer = (packetBufferWritePointer + 1)
					& PACKET_BUFFER_MASK;
			packet = &packets[packetBufferWritePointer];
			goto CHECK_FOR_SPACE;
		}
		goto NIBBLE_9;
	}
	packet->buffer[5] = nibbleIn;
	NIBBLE_10: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_10;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_10;
		// Glitch protection
	}
	packet->buffer[5] |= nibbleIn << 4;
	NIBBLE_11: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_11;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_11;
		// Glitch protection
	}
	packet->buffer[6] = nibbleIn;
	NIBBLE_12: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_12;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_12;
		// Glitch protection
	}
	packet->buffer[6] |= nibbleIn << 4;
	NIBBLE_13: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_13;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_13;
		// Glitch protection
	}
	packet->buffer[7] = nibbleIn;
	NIBBLE_14: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_14;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_14;
		// Glitch protection
	}
	packet->buffer[7] |= nibbleIn << 4;
	NIBBLE_15: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_15;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_15;
		// Glitch protection
	}
	packet->buffer[8] = nibbleIn;
	NIBBLE_16: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_16;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) {
		if (diff == EOP)
			goto NEW_PACKET;
		//Something odd happened, dropping packet
		else
			goto NIBBLE_16;
		// Glitch protection
	}
	packet->buffer[8] |= nibbleIn << 4;
	NIBBLE_17: current_input_state = (LPC_GPIO_PORT->PIN[1] >> 1)
			& (uint32_t) 0x007F;
	if (current_input_state == last_input_state)
		goto NIBBLE_17;
	parseNibble();
	if (nibbleIn == INVALID_VALUE) { //Read an EOP
		if (diff == EOP) {
			packetBufferWritePointer = (packetBufferWritePointer + 1)
					& PACKET_BUFFER_MASK;
			packet = &packets[packetBufferWritePointer];
			goto CHECK_FOR_SPACE;
		}
		goto NIBBLE_17;
	}
	goto NEW_PACKET;

	return 0;
}

#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {
	volatile static int i = 0;
	while (1) {
		i++;
	}
}

#endif
